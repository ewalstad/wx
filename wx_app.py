#!/user/bin/env python
# -*- coding: utf-8 -*-
"""
Weather data viewer app
=======================

Assumes that the image files are located in::

  ./static/cam/<year>/<month>/<hhmm.jpg>

"""
from __future__ import with_statement
import os
from os import path, listdir
from datetime import datetime, timedelta
from werkzeug.wsgi import SharedDataMiddleware
from flask import Flask, render_template
#from weather_station.make_movie import (
#        dirpath_to_datetime, get_wx_data, Classtionary)

app = Flask(__name__)
app.debug = True

# ./static/cam
REL_CAM_DIR = "static/cam"
THIS_DIR = path.dirname(path.abspath(__file__))
ABS_CAM_DIR = path.join(THIS_DIR, REL_CAM_DIR)
WX_FILE = path.join(THIS_DIR, 'static/open2300.log')


class Classtionary(dict):
    """A Class, er, dictionary, um, attributes-as-keys thingy.

    >>> c = Classtionary(foo="bar")
    >>> d = dict(foo="bar")
    >>> c, d
    ({'foo': 'bar'}, {'foo': 'bar'})
    >>> c.fizz = "bang"
    >>> d.fizz = "bang"
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
    AttributeError: 'dict' object has no attribute 'fizz'
    >>> d['fizz'] = "bang"
    >>> c['fizz']
    'bang'
    >>> c.fizz
    'bang'
    >>> d['fizz']
    'bang'
    >>> d.fizz
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
    AttributeError: 'dict' object has no attribute 'fizz'
    """

    def __init__(self, **kwargs):
        self.__dict__ = self
        self.update(kwargs)


def get_wx_data(timestamp, wx_file_obj, seconds_resolution=None):
    """Returns a dictionary of weather data from the file object as close in
    time as possible to the timestamp.  We will search for weather data within
    seconds_resolution of timestamp.

    This function uses a binary search if its the first pass through the file
    or if the current record is past the timestamp we are looking for.
    Otherwise the search proceeds one row at a time through the weather data
    file.

    Assumes the weather file is sorted by Timestamp in ascending order and that
    the weather file format is similar to this (from a LaCrosse weather
    station):
    Timestamp Date Time Temp_indoor Temp_outdoor Dewpoint Rel_humidity_indoor Rel_humidity_outdoor Wind_speed Wind_direction-degrees Wind_direction_text Wind_chill Rain_1h Rain_24h Rain_total Rel_pressure Tendency Forecast
    20100718111016 2010-Jul-18 11:10:16 91.6 84.9 56.4 35 38 1.1 247.5 WSW 84.9 0.00 0.00 7.97 25.189 Rising Sunny
    or
    Timestamp Date Time Temp_indoor Temp_outdoor Dewpoint Rel_humidity_indoor Rel_humidity_outdoor Wind_speed Wind_direction-degrees Wind_direction_text Wind_chill Rain_1h Rain_24h Rain_total Rel_pressure
    20100718014500 2010-Jul-18 01:45:00 67.6 64.2 51.4 49 63 0.0 112.5 ESE 64.2 7.97 25.207

    >>> import datetime
    >>> date_fmt = '%Y%m%d%H%M%S'
    >>> dt_day = datetime.datetime(2010, 07, 18, 11, 10)
    >>> dt_next_row = datetime.datetime(2010, 07, 18, 11, 15)
    >>> dt_night = datetime.datetime(2010, 07, 18, 01, 45)
    >>> dt_missing_row = datetime.datetime(2010, 7, 17, 13, 0)
    >>> dt_past_end = datetime.datetime(2110, 1, 1, 0, 0)
    >>> test_wx = open('./tests/test_wx_data.log', 'r')

    >>> wx_data = get_wx_data(dt_day, test_wx)
    >>> keys = wx_data.keys()
    >>> keys.sort()
    >>> print(keys)
    ['Date', 'Dewpoint', 'Forecast', 'Rain_1h', 'Rain_24h', 'Rain_total', 'Rel_humidity_indoor', 'Rel_humidity_outdoor', 'Rel_pressure', 'Temp_indoor', 'Temp_outdoor', 'Tendency', 'Time', 'Timestamp', 'Wind_chill', 'Wind_direction-degrees', 'Wind_direction_text', 'Wind_speed']
    >>> get_wx_data(dt_day, test_wx)['Timestamp']
    datetime.datetime(2010, 7, 18, 11, 10, 16)
    >>> get_wx_data(dt_next_row, test_wx)['Timestamp']
    datetime.datetime(2010, 7, 18, 11, 15, 14)
    >>> get_wx_data(dt_night, test_wx).Timestamp
    datetime.datetime(2010, 7, 18, 1, 45)
    >>> get_wx_data(dt_missing_row, test_wx).Timestamp
    datetime.datetime(2010, 7, 17, 13, 10, 25)
    >>> get_wx_data(dt_day, test_wx).Rel_pressure
    '25.189'
    >>> get_wx_data(dt_day, test_wx).Tendency
    'Rising'
    >>> get_wx_data(dt_day, test_wx).Forecast
    'Sunny'
    >>> get_wx_data(dt_past_end, test_wx).Timestamp
    Error parsing the Timestamp from this wx file line:
    ''
    """
    if seconds_resolution is None:
        # we take a weather reading every 5 minutes, 300 seconds.  We want our
        # found row to be within 1/2 of that, or 150 seconds.
        seconds_resolution = 150

    field_names = (
        'Timestamp', 'Date', 'Time', 'Temp_indoor', 'Temp_outdoor', 'Dewpoint',
        'Rel_humidity_indoor', 'Rel_humidity_outdoor', 'Wind_speed',
        'Wind_direction-degrees', 'Wind_direction_text', 'Wind_chill',
        'Rain_1h', 'Rain_24h', 'Rain_total', 'Rel_pressure', 'Tendency',
        'Forecast',
    )

    data = {}

    def parse_line(line):
        """Parse a line and return a dictionary"""
        data = line.split()
        if len(data) < len(field_names):
            # This is a nighttime line, insert a some null data in the missing
            # fields
            # Tendency and Forecast
            data.extend([None, None])
            # Rain_1h and Rain_24h
            data.insert(10, None)
            data.insert(11, None)
        ret_val = Classtionary(**dict(zip(field_names, data)))
        if not ret_val.Timestamp:
            ret_val.Timestamp = datetime.datetime.now()
        # Make the first item a datetime object
        try:
            ret_val.Timestamp = datetime.datetime.strptime(
                ret_val.Timestamp, '%Y%m%d%H%M%S')
        except TypeError, e:
            # Trouble with this weather data, return whatever data we have
            print(
                "Error parsing the Timestamp from this wx file line:\n'%s'"
                % line)
        return ret_val
    def is_match(wx_data_line):
        """Returns True if wx_data_line is within seconds_resolution of
        timestamp
        """
        try:
            seconds_diff = abs(timestamp-wx_data_line.Timestamp).seconds
            if bool(seconds_diff <= seconds_resolution):
                return True
            else:
                return False
        except TypeError:
            print(
                "TypeError when calculating the time distance to our target "
                "timestamp.  timestamp: '%s'; wx_data.Timestamp: '%s'"
                % (timestamp, wx_data.Timestamp))
            return False

    def search(start_pos, end_pos, idx=None):
        """Return True if we've found the data line we are looking for."""
        search_pos = start_pos + ((end_pos - start_pos) // 2)
        wx_file_obj.seek(search_pos)
        if idx is None:
            idx = 0
        # This will likely return a partial line...
        line = wx_file_obj.readline()
        # ...so we'll look at the following line
        line = wx_file_obj.readline()
        d = parse_line(line)
        if not d.get('Timestamp', False):
            return d
        if is_match(d):
            # Found it, return the data line
            return d
        else:
            # Is this as close as we can get?
            if idx and idx > 50:
                # We've recursed 50 levels, probably as close as we can get
                # so just return this row
                return d

            # Keep looking
            cur_pos = wx_file_obj.tell() - (len(line) + 1)
            if timestamp < d['Timestamp']:
                # search earlier in the file
                return search(start_pos, cur_pos, idx+1)
            else:
                # search later in the file
                return search(cur_pos, end_pos, idx+1)
    start_pos = wx_file_obj.tell()
    pth = os.path.abspath(wx_file_obj.name)
    file_size = os.path.getsize(pth)

    if start_pos == 0:
        # We are at the beginning of the file
        # Search for the datestamp with a binary search
        end_pos = file_size
        data = search(start_pos, end_pos)
    else:
        # Assume we've already found a line and are looking for the next line
        while True:
            line = wx_file_obj.readline()
            if not line:
                # We've reached the end of the file, try a binary search
                # instead
                end_pos = file_size
                start_pos = 0
                data = search(start_pos, end_pos)
                break
            else:
                data = parse_line(line)
                if is_match(data):
                    # Found it
                    break
                elif data['Timestamp'] > timestamp:
                    # We're past the line we are looking for so fall back to a
                    # binary search
                    end_pos = wx_file_obj.tell()
                    start_pos = 0
                    data = search(start_pos, end_pos)
                    break
    return data


def dirpath_to_datetime(pth):
    """Converts dirpath to a datetime object
    Assumes the dirpath includes
    /optional/prefix/year/month/day/hhmm.jp[e]g
    """
    fn = os.path.splitext(os.path.basename(pth))[0]
    hour = int(fn[:2])
    minute = int(fn[2:])

    parts = os.path.dirname(pth).split('/')
    parts.reverse()
    day = int(parts[0])
    month = int(parts[1])
    year = int(parts[2])
    dt = datetime(year, month, day, hour, minute)
    return dt


def latest_images(max_images=40, abs_cam_dir=ABS_CAM_DIR, rel_cam_dir=REL_CAM_DIR):
    """Return a list of the relative paths to the latest available webcam
    images.  Returns a list inclduing no more than max_images items.
    If an image can't be found within the last 90 days, returns False
    """
    the_date = datetime.now()
    ret_val = []
    for n in range(90):
        # Work back 90 days until we find image files
        d = the_date - timedelta(n)
        yr = str(d.year)
        mo = ("%0.2d" % d.month)
        day = ("%0.2d" % d.day)
        pth = path.join(abs_cam_dir, yr, mo, day)
        try:
            fs = listdir(pth)
        except OSError:
            continue
        fs.sort()
        if fs and path.exists(path.join(pth, fs[-1])):
            # The relative path to the latest file
            for f in fs[-max_images:]:
                HHMM = f.split(".")[0]
                h, m = HHMM[:2], HHMM[2:]
                ret_val.append(
                    Classtionary(
                        pth=path.join(rel_cam_dir, yr, mo, day, f),
                        filename=f,
                        caption="%s/%s/%s %s:%s" % (yr, mo, day, h, m),
                    )
                )
            return ret_val
    return False


def latest_image(abs_cam_dir=ABS_CAM_DIR, rel_cam_dir=REL_CAM_DIR):
    """Return the relative path to the latest available webcam image.
    If an image can't be found within the last 90 days, returns False
    """
    the_date = datetime.now()
    for n in range(90):
        # Work back 90 days until we find an image file
        d = the_date - timedelta(n)
        yr = str(d.year)
        mo = ("%0.2d" % d.month)
        day = ("%0.2d" % d.day)
        pth = path.join(abs_cam_dir, yr, mo, day)
        try:
            fs = listdir(pth)
        except OSError:
            continue
        fs.sort()
        if fs and path.exists(path.join(pth, fs[-1])):
            # The relative path to the latest file
            f = fs[-1]
            return path.join(rel_cam_dir, yr, mo, day, f)
    return False


def parse_line(line):
    """Parse a line and return a dictionary"""
    data = line.split()
    field_names = (
        'Timestamp', 'Date', 'Time', 'Temp_indoor', 'Temp_outdoor', 
        'Dewpoint', 'Rel_humidity_indoor', 'Rel_humidity_outdoor', 
        'Wind_speed', 'Wind_direction-degrees', 'Wind_direction_text',
        'Wind_chill', 'Rain_1h', 'Rain_24h', 'Rain_total', 'Rel_pressure',
        'Tendency', 'Forecast',
    )
    if len(data) < len(field_names):
        # This is a nighttime line, insert a some null data in the missing
        # fields
        # Tendency and Forecast
        data.extend([None, None])
        # Rain_1h and Rain_24h
        data.insert(10, None)
        data.insert(11, None)
    ret_val = Classtionary(**dict(zip(field_names, data)))
    # Make the first item a datetime object
    try:
        ret_val.Timestamp = datetime.strptime(
            ret_val.Timestamp, '%Y%m%d%H%M%S')
    except TypeError, e:
        # Trouble with this weather data, return whatever data we have
        print(
            "Error parsing the Timestamp from this wx file line:\n'%s'"
            % line)
    return ret_val

def latest_wx_summary(wx_file=WX_FILE):
    """Returns a dictionary containing the latests weather data."""
    # look up associated weather data
    wx_file_obj = open(WX_FILE, 'r')
    with wx_file_obj:
        try:
            wx_file_obj.seek(-1000, os.SEEK_END)
        except IOError:
            wx_file_obj.seek(-1, os.SEEK_END)
        lines = wx_file_obj.readlines()
        last_line = lines[-1]
        wx_data = parse_line(last_line) 
    return wx_data


@app.route('/')
def index():
    """Index page for the application"""
    latest_img = latest_image()
    latest_image_datetime = dirpath_to_datetime(path.abspath(latest_img))
    return render_template('index.html', 
        **dict(
            latest_image=latest_img,
            latest_images=latest_images(),
            latest_image_datetime=latest_image_datetime,
            header_text="RRR Weather""",
            wx_data=latest_wx_summary(),
        )
    )


if app.config['DEBUG']:
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
        '/static': 'static',
    })

if __name__ == '__main__':
    app.run(host="0.0.0.0")

