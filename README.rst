================
Weather Data App
================

App for viewing images and weather data


Quick Start
===========

::

   mkvirtualenv wx
   git clone git@bitbucket.org:ewalstad/wx.git
   cd wx
   pip install -r requirements.txt
   cd static
   ln -s /path/to/weather_station/cam
   ln -s /path/to/weather_station/open2300.log
   cd ../
   python ./wx_app.py

