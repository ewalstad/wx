Deploy to Nihiwatu (which runs Ubuntu Hardy (8.04LTS))
======================================================

See also: 
  
  * http://flask.pocoo.org/docs/deploying/uwsgi/
  * http://projects.unbit.it/uwsgi/wiki/Quickstart#NginxuWSGIflask
  * http://wiki.nginx.org/GettingStarted
  * http://brandonkonkle.com/blog/2010/sep/14/django-uwsgi-and-nginx/


Push code out to the production server:
+++++++++++++++++++++++++++++++++++++++

::
        
        workon wx_env && fab -f wx/etc/fabfile.py nihiwatu deploy

Configure the servers:
+++++++++++++++++++++

nginx
-----

    1. Install system-wide dependencies::

        sudo apt-get install libpcre3-dev

    2. Install nginx from source::

        cd /tmp
        wget http://nginx.org/download/nginx-1.0.14.tar.gz
        tar xvzf nginx-1.0.14.tar.gz 
        cd nginx-1.0.14/
        ./configure
        make
        sudo make install
  
    3. Configure nginx::
        
        sudo cp /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf_$(nowdate)
        sudo vim /usr/local/nginx/conf/nginx.conf

        $ diff -Bub /usr/local/nginx/conf/nginx.conf_201204021754 /usr/local/nginx/conf/nginx.conf
        --- /usr/local/nginx/conf/nginx.conf_201204021754       2012-04-02 17:54:30.000000000 -0700
        +++ /usr/local/nginx/conf/nginx.conf    2012-04-02 19:46:01.000000000 -0700
        @@ -4,9 +4,9 @@
         
         #error_log  logs/error.log;
         #error_log  logs/error.log  notice;
        -#error_log  logs/error.log  info;
        +error_log  logs/error.log  info;
         
        -#pid        logs/nginx.pid;
        +pid        logs/nginx.pid;
         
         events {
        @@ -33,16 +33,21 @@
             #gzip  on;
         
             server {
        -        listen       80;
        -        server_name  localhost;
        +        listen       8888;
        +        server_name  rrrwx.ericwalstad.net;
         
                 #charset koi8-r;
         
                 #access_log  logs/host.access.log  main;
         
        -        location / {
        -            root   html;
        -            index  index.html index.htm;
        +        #location / {
        +        #    root   html;
        +        #    index  index.html index.htm;
        +        #}
        +       location / { try_files $uri @wx_app; }
        +       location @wx_app {
        +               include uwsgi_params;
        +               uwsgi_pass 127.0.0.1:3031;
                 }
         
                 #error_page  404              /404.html;

    4. Configure nginx to start at boot: edit /etc/init.d/nginx::

        #! /bin/sh

        ### BEGIN INIT INFO
        # Provides:          nginx
        # Required-Start:    $all
        # Required-Stop:     $all
        # Default-Start:     2 3 4 5
        # Default-Stop:      0 1 6
        # Short-Description: starts the nginx web server
        # Description:       starts nginx using start-stop-daemon
        ### END INIT INFO

        PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
        DAEMON=/usr/local/nginx/sbin/nginx
        NAME=nginx
        DESC=nginx

        test -x $DAEMON || exit 0

        # Include nginx defaults if available
        if [ -f /etc/default/nginx ] ; then
            . /etc/default/nginx
        fi

        set -e

        case "$1" in
          start)
            echo -n "Starting $DESC: "
            start-stop-daemon --start --quiet --pidfile /usr/local/nginx/logs/$NAME.pid \
                --exec $DAEMON -- $DAEMON_OPTS
            echo "$NAME."
            ;;
          stop)
            echo -n "Stopping $DESC: "
            start-stop-daemon --stop --quiet --pidfile /usr/local/nginx/logs/$NAME.pid \
                --exec $DAEMON
            echo "$NAME."
            ;;
          restart|force-reload)
            echo -n "Restarting $DESC: "
            start-stop-daemon --stop --quiet --pidfile \
                /usr/local/nginx/logs/$NAME.pid --exec $DAEMON
            sleep 1
            start-stop-daemon --start --quiet --pidfile \
                /usr/local/nginx/logs/$NAME.pid --exec $DAEMON -- $DAEMON_OPTS
            echo "$NAME."
            ;;
          reload)
              echo -n "Reloading $DESC configuration: "
              start-stop-daemon --stop --signal HUP --quiet --pidfile /usr/local/nginx/logs/$NAME.pid \
                  --exec $DAEMON
              echo "$NAME."
              ;;
          *)
            N=/etc/init.d/$NAME
            echo "Usage: $N {start|stop|restart|reload|force-reload}" >&2
            exit 1
            ;;
        esac

        exit 0

    5. Start/Stop nginx::

        sudo /etc/init.d/nginx [start|stop]


uWSGI
-----

    1. Install uWSGI::

        cd /tmp
        wget http://projects.unbit.it/downloads/uwsgi-latest.tar.gz
        tar -xzf uwsgi-latest.tar.gz
        cd uwsgi-1.1.2/
        make
        sudo cp uwsgi /usr/local/sbin

    2. Run uWSGI::


        uwsgi \
            --master \
            --socket 127.0.0.1:3031 \
            --pythonpath /home/ewalstad/virtualenvs/wx_env/lib/python2.5/site-packages/ \
            --pythonpath /home/ewalstad/var/ew_trunk/property \
            --pythonpath /usr/lib/python2.5/site-packages/ \
            --file /home/ewalstad/virtualenvs/wx_env/wx/wx_app.py \
            --callable app & 

