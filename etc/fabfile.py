"""Fabric deployment file for wx_app

Assumes you are using virtualenv and virtualenvwrapper with a 'wx_env' virtualenv

Example usage:
workon wx_env
fab -f wx/etc/fabfile.py nihiwatu deploy
fab -f wx/etc/fabfile.py --list
"""
import os
import fabric
from fabric.api import env, settings, local, lcd, cd, run, sudo
from fabric.contrib.console import confirm


env.port = 7522
env.hosts = []
this_dir = os.path.abspath(os.path.dirname(__file__))
repo_dir = os.path.abspath(os.path.join(this_dir, '../'))
nihi = "nihiwatu.ericwalstad.net"
env.uwsgi_start = {
    nihi: """ 
        uwsgi \
            --master \
            --socket 127.0.0.1:3031 \
            --pythonpath /home/ewalstad/virtualenvs/wx_env/lib/python2.5/site-packages/ \
            --pythonpath /home/ewalstad/var/ew_trunk/property \
            --pythonpath /usr/lib/python2.5/site-packages/ \
            --file /home/ewalstad/virtualenvs/wx_env/wx/wx_app.py \
            --callable app \
            --processes 3 \
            --pidfile /tmp/uwsgi.pid \
            --log-syslog wx_app \
            --idle 90 \
            --single-interpreter \
            &
    """,
}


def nihiwatu():
    """Use the nihiwatu server"""
    env.hosts = [
        '%s@%s:%s' % (env.local_user, nihi, env.port)]

def push():
    """Push the code out to bitbucket"""
    with lcd(repo_dir):
        local("git push origin master")

def deploy():
    """Deploy the code"""
    with cd("/home/ewalstad/virtualenvs/wx_env/wx"):
        run("git pull")

    with cd("/home/ewalstad/var/ew_trunk/property/weather_station/"):
        run("svn up")

    with settings(warn_only=True):
        run("killall -9 uwsgi")
    run(env.uwsgi_start[env.host]) 
    sudo("/etc/init.d/nginx stop && sleep 1; /etc/init.d/nginx start")

